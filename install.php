<?php

/**
 * @file
 * Initiates installation process.
 */

/**
 * Root directory of Drupal installation.
 */
define('DRUPAL_ROOT', getcwd());

/**
 * Global flag to indicate that site is in installation mode.
 */
define('MAINTENANCE_MODE', 'install');

// Exit early if running an incompatible PHP version to avoid fatal errors.
// The minimum version is specified explicitly, as DRUPAL_MINIMUM_PHP is not
// yet available. It is defined in bootstrap.inc, but it is not possible to
// load that file yet as it would cause a fatal error on older versions of PHP.
if (version_compare(PHP_VERSION, '5.3.2') < 0) {
  print 'Your PHP installation is too old. Drupal requires at least PHP 5.3.2. See the <a href="http://drupal.org/requirements">system requirements</a> page for more information.';
  exit;
}

// Explicitely load all the required files.
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/includes/cache.inc';
require_once DRUPAL_ROOT . '/includes/common.inc';
require_once DRUPAL_ROOT . '/includes/database/database.inc';
require_once DRUPAL_ROOT . '/includes/file.inc';
require_once DRUPAL_ROOT . '/includes/install.inc';
require_once DRUPAL_ROOT . '/includes/lock.inc';
require_once DRUPAL_ROOT . '/includes/module.inc';
require_once DRUPAL_ROOT . '/includes/menu.inc';
require_once DRUPAL_ROOT . '/modules/system/system.module';

// Define the temporary database.
$databases = _drupal_bootstrap_database_temp();

// Remove any pre-existing temporary database.
if (file_exists($databases['default']['default']['database'])) {
  unlink($databases['default']['default']['database']);
}

// Install the minimal modules required to successfully run Drupal. System and
// user are required to provide all basic systems like menu router and access
// checks, but in order to perform drupal_flush_all_caches() the node module
// is required so its dependencies must also be included.
$modules = array('system', 'user', 'filter', 'entity', 'node');

$insert = db_insert('system')->fields(array('filename', 'name', 'type', 'owner', 'status', 'schema_version', 'bootstrap'));
foreach ($modules as $module) {
  $path = "modules/$module/$module";
  if (file_exists("$path.install")) {
    require_once "$path.install";
    if (function_exists($function = $module . '_schema')) {
      // Manually perform since module system in not available.
      $schema = $function();
      _drupal_schema_initialize($schema, $module, FALSE);
      foreach ($schema as $name => $table) {
        db_create_table($name, $table);
      }
    }
  }

  $insert->values(array(
    'filename' => "$path.module",
    'name' => $module,
    'type' => 'module',
    'owner' => '',
    'status' => 1,
    'schema_version' => 0,
    'bootstrap' => 0,
  ));
}
$insert->execute();

registry_rebuild();
system_rebuild_theme_data();
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// Enable install module and theme to be used during installation.
module_enable(array('install'));
theme_enable(array('bartik'));
variable_set('theme_default', 'bartik');

// Redirect to install module form.
drupal_goto('install/wizard');
